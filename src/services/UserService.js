import User from '../models/User'

import { apiService } from '../shared/APIService'
import { storageService } from '../shared/StorageService'
import { LS_KEYS } from '../constants'

class UserService {

    getUsers() {
        return !!storageService.read(LS_KEYS.users) ?
            this.loadUsers() : this.fetchUsers()
    }

    fetchUsers() {
        const { adaptUsers } = this
        return apiService
            .get('?results=9')
            .then(data => data.results)
            .then((usersData) => {
                const userList = adaptUsers(usersData)

                storageService.save(LS_KEYS.users, userList)
                return userList
            })
    }

    loadUsers() {
        const myUsers = storageService.read(LS_KEYS.users)

        return new Promise((resolve, reject) => {
            resolve(this.adaptUsers(myUsers))
        })
    }

    adaptUsers(usersList) {
        return usersList
            .map((user) => {
                const { name, email, gender, dob, picture } = user
                return new User(name, email, dob, gender, picture)
            })
    }
}

export const userService = new UserService()
