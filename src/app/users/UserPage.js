import React, { Component } from 'react'
import UserList from './UserList'
import { userService } from '../../services/UserService'

class UserPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            users: []
        }
    }

    componentWillReceiveProps(nextProps) {
        // If view is changed don't update the state
        // same user will be shown
        if (nextProps.isGridMode !== this.props.isGridMode) {
            return
        }

        // Should fetch new users form the API
        if (nextProps.shouldFetchUsers !== this.props.shouldFetchUsers) {
            // Fetch new user from the API using fetchUsers() method
            // instead of getUsers()
            userService.fetchUsers()
                .then((users) => {
                    this.setState({ users })
                })
        }
    }

    componentDidMount() {
        // On mount get users from storage or from
        // the API
        userService.getUsers()
            .then((users) => {
                this.setState({ users })
            })
    }

    render() {
        const { users } = this.state
        const { isGridMode } = this.props
        return (
            <div className="container">
                <UserList isGridMode={isGridMode} users={users} />
            </div>
        )
    }
}

export default UserPage
