import React from 'react'
import PropTypes from 'prop-types'

// Only for checking instance in props
import User from '../../models/User'

const UserCardItem = ({ user }) => {
    const { name, dob, picture } = user
    const redColor = user.isFemale() ? 'red lighten-5' : ''
    const firstName = name.first

    return (
        <div className="col s12 m4">
            <div className={`card hoverable ${redColor}`}>
                <div className="card-image">
                    <img src={picture.large} alt={firstName} />
                    <span className="card-title">{firstName}</span>
                </div>
                <div className="card-content">
                    <p>{user.getHiddenEmail()}</p>
                    <p>Birth date: {dob}</p>
                </div>
            </div>
        </div>
    )
}

UserCardItem.propTypes = {
    user: PropTypes.instanceOf(User)
}

export default UserCardItem
