import React from 'react'
import PropTypes from 'prop-types'

const UserListItem = ({ user }) => {
    const { name, picture, dob } = user
    const firstName = name.first
    const redColor = user.isFemale() ? 'red lighten-5' : ''

    return (
        <li className={`collection-item avatar hoverable ${redColor}`}>
            <img src={picture.medium} alt="" className="circle" />
            <span className="title">{firstName}</span>
            <p className="valign-wrapper">
                <i className="material-icons">email</i> {user.getHiddenEmail()}
            </p>
            <p className="valign-wrapper">
                <i className="material-icons">cake</i>
                {dob}
            </p>
        </li>
    )
}

UserListItem.propTypes = {
    user: PropTypes.object.isRequired
}

export default UserListItem
