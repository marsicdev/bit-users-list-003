import React from 'react'

import Header from './common/Header'
import Footer from './common/Footer'
import UserPage from './users/UserPage'

class Home extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            gridMode: false,
            refreshUsers: false
        }
    }

    onViewModeChange = event => {
        event.preventDefault()

        const gridMode = !this.state.gridMode
        this.setState({ gridMode })
    }

    onRefreshHandler = () => {
        const { refreshUsers } = this.state
        this.setState({ refreshUsers: !refreshUsers })
    }

    render() {
        const { onViewModeChange, onRefreshHandler } = this
        const { gridMode, refreshUsers } = this.state

        return (
            <React.Fragment>
                <Header changeViewMode={onViewModeChange}
                    onRefresh={onRefreshHandler}
                    gridMode={gridMode} />
                <main>
                    <UserPage
                        isGridMode={gridMode}
                        shouldFetchUsers={refreshUsers} />
                </main>
                <Footer />
            </React.Fragment>
        )
    }

}

export default Home