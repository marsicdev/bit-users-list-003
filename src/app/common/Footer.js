import React from 'react'

const Footer = () => (
    <footer className="page-footer">
        <div className="footer-copyright">
            <div className="container">
                © {new Date().getFullYear()} BIT
            </div>
        </div>
    </footer>
)

export default Footer
