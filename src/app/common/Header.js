import React from 'react'

const Header = ({ gridMode, changeViewMode, onRefresh }) => (
    <header>
        <nav>
            <div className="container nav-wrapper">
                <a className="brand-logo">USERS</a>
                <ul className="right">
                    <li>
                        <a onClick={onRefresh}>
                            <i className="material-icons">refresh</i>
                        </a>
                    </li>
                    <li onClick={changeViewMode}>
                        <a>
                            <i className="material-icons">
                                {gridMode ? 'view_list' : 'view_module'}
                            </i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
)

export default Header
