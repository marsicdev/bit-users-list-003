import React, { Component } from 'react'
import './App.css'

import { Switch, Route } from 'react-router-dom'
import Home from './Home'

import 'materialize-css/dist/css/materialize.min.css'

class App extends Component {
    render() {
        return (
            <Switch>
                <Route exact path={'/'} component={Home} />
            </Switch>
        )
    }
}

export default App
