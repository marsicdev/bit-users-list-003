export const BASE_URL = 'https://randomuser.me/api'

export const LS_KEYS = Object.freeze({
    users: 'ALL_USERS'
})
