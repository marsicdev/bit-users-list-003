import { BASE_URL } from '../constants'

class APIService {

    get(path) {
        const requestUrl = `${BASE_URL}/${path}` 
        return fetch(requestUrl)
            .then(response => response.json())
    }

    post(path, data) {

        const requestUrl = `${BASE_URL}/${path}` 

        // Default options are marked with *
        const requestOptions = {
            body: JSON.stringify(data), // must match 'Content-Type' header
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST' // *GET, POST, PUT, DELETE, etc.
        }

        return fetch(requestUrl, requestOptions)
            .then(response => response.json()) // parses response to JSON
    }
}

export const apiService = new APIService()
