export default class User {
    constructor(name, email, dob, gender, picture) {
        this.name = name
        this.email = email
        this.dob = dob
        this.gender = gender
        this.picture = picture
    }

    getHiddenEmail() {
        const dots = '...'
        const [mail, domain] = this.email.split('@')
        const start = mail.substring(0, 3)
        const end = mail.substring(mail.length - 1, mail.length - 1 - 3)
        return `${start + dots + end}@${domain}`
    }

    isFemale() {
        return this.gender === 'female'
    }
}
